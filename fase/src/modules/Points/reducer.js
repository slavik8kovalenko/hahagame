export const NAME = 'POINTS'
export const types = {
  GET_SUBSCRIPTION_REQUEST: `${NAME}/GET_SUBSCRIPTION_REQUEST`,
}

export const initialState = {
  header: 'Sukaaaaa',
  matrix: []
}

export function reducer(state = initialState, action) {
  switch (action.type) {
    case "SET_MATRIX": {
      return {
        ...state,
        matrix: action.matrix
      }
    }
    default:
      return { ...state }
  }
}
