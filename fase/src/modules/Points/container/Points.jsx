import React from 'react';
import { connect } from 'react-redux'
import { NAME as POINTS_NAME } from '../reducer'
import { bindActionCreators } from 'redux'
import { actions as pointsActions } from '../actions'

function mapStateToProps(state) {
  return {
    ...state[POINTS_NAME]
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...pointsActions
}, dispatch)

class Points extends React.Component {

  constructor(props) {
    super(props);
    props.renderTable();
    console.log('na rendere: ', props.matrix)
    // Web_Dashboard_Amazonbtn To DO
  }

  componentDidMount() {
  }
  render() {
    return (
      <>
        <table>
          <tbody>
            {this.props.matrix.map((x, keyX) => {
              return (
                <tr key={keyX}>
                  {x.map((item, keyY) => {
                    return (
                      <td onClick={() => {this.props.sendPoint(keyX,keyY,5)}} key={keyX + '-' + keyY}>{item}</td>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
      </>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Points)
