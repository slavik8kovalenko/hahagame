import thunk from 'redux-thunk'
import { createStore, applyMiddleware, combineReducers } from 'redux'
//import { reducer as schulteLettersReducer, NAME as SCHULTELETTERS_NAME } from './modules/SchulteLetters/reducer'
//import { reducer as wordPairsReducer, NAME as WORD_PAIRS_NAME } from './modules/WordPairs/reducer'
//import { reducer as letterGridSearch, NAME as GRID_SEARCH_NAME } from './modules/GridSearch/reducer'
//import { reducer as dashboard, NAME as DASHBOARD_NAME } from './modules/Dashboard/reducer'
import { reducer as points, NAME as POINTS_NAME } from './modules/Points/reducer'

const appReducer = combineReducers({
  [POINTS_NAME]: points
})

const store = createStore(
  appReducer,
  applyMiddleware(thunk)
)

export default store
