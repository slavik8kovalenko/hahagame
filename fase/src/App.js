import React from 'react';
import Points from './modules/Points/container/Points'
import './App.css';

function App() {
  return (
    <div className="App">
      <Points></Points>
    </div>
  );
}

export default App;
