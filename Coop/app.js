const express = require('express')
const cors = require('cors');
const app = express()
app.use(cors())
const port = 3000
let mas = []
for(let i = 0; i<10;i++) {
  mas[i] = []
  for(let j =0; j<10; j++) {
    mas[i][j] = 0
  }
}
console.log(mas);
/*
app.get('/', (req, res) => {
  res.send('Hello World!')
})
*/
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

app.get('/setPoint', (req,res) => {
  if(req.query.x && req.query.y && req.query.value) {
    mas[req.query.x][req.query.y] = req.query.value
  }
  res.send(JSON.stringify(mas))
})